from prometheus_client import start_http_server, Gauge, Info
import speedtest
import time
import os


server_info = Info('server_info', "Testing Server Information")
download_speed_metric = Gauge('download_speed', 'Download speed in Mbps')
upload_speed_metric = Gauge('upload_speed', 'Upload speed in Mbps')
latency_metric = Gauge('latency', 'Latency in milliseconds')
latitude_metric = Gauge('latitude', 'Latitude of Testing Server')
longitude_metric = Gauge('longitude', 'Longitude of Testing Server')

def test_speed():
    st = speedtest.Speedtest()
    download_speed = st.download() / 1_024_000
    upload_speed = st.upload() / 1_024_000
    latency = st.results.ping

    server_latitude = st.results.server.get('lat', 0)
    server_longitude = st.results.server.get('lon', 0)
    server = st.results.server

    return download_speed, upload_speed, latency, server_latitude, server_longitude, {key: str(val) for key, val in server.items()}

def update_metrics():
    download_speed, upload_speed, latency, latitude, longitude, server = test_speed()

    download_speed_metric.set(download_speed)
    upload_speed_metric.set(upload_speed)
    latency_metric.set(latency)
    server_info.info(server)

    latitude_metric.set(latitude)
    longitude_metric.set(longitude)


if __name__ == "__main__":
    start_http_server(9100)

    while True:
        update_metrics()
        time.sleep(5)
